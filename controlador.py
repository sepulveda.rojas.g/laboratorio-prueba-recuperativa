"""
La clase Controlador() hereda los parametros,
el constructor y los metodos de la clase Contactos
que es la que nos permite utilizar la lógica de 
programación en la clase de Contactos()
"""
import contactos as con

class Controlador(con.Contactos):

    def nuevoContacto(self, nuevo):
        return self.setContacto(nuevo)
      
    def tomarContacto(self):
        return self.getContacto()
      
    def tomarContactos(self):
        return self.getContactos()

    def validarContacto(self, numero):
        return self.validaContacto(numero)
