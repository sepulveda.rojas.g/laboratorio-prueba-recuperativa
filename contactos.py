"""
la clase Contactos() es la que posee la 
lógica de cómo almacenar nuevos contactos
a la lista de contactos

se puede consultar un contacto y también se
puede validar los contactos a partir del 
número de teléfono, si existe o no existe 
dentro del dentro de la lista de contactos
"""

class Contactos:
   
    def __init__(self, contactos):
        numero = str()
        self.contactos = contactos

    def setContacto(self, nuevo):
        #se construye un nuevo contacto
        self.contacto = {
            'numero': nuevo['numero'],
            'nombre': nuevo['nombre'],
            'cargo': nuevo['cargo']
        }
        #se adiciona a la lista de contactos
        self.contactos.append(self.contacto)
        #returna el resultado de la operación
        return "contacto agregado exitosamente"
      
    def getContacto(self):
        #devuelve el diccionario del contacto actual
        return self.contacto
      
    def getContactos(self):
        #devuelve la lista de diccionarios de todos los contactos
        return self.contactos

    def validaContacto(self, numero):
        #valida si un contacto existe a partir de su numero de telefono
        contactos = self.contactos
        for contacto in contactos:
            if(contacto['numero'] == numero):
                self.contacto = contacto
                return "si existe"
        return "no existe"