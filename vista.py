"""
La vista es la que nos permite interactuar 
con la interfaz gráfica utilizando las 
funciones del controlador.
"""
import controlador as cont

#datos en formato json
agenda = [
    {
       'numero': '3228858439',
       'nombre': 'oscar',
       'cargo': 'supervisor',
       'direccion': ''
    },
    {
       'numero': '3228858459',
       'nombre': 'oscar',
       'cargo': 'supervisor',
       'direccion': ''
    }
]

#datos de ejemplo para agregar un contacto
nuevo = {
    'numero': '3228858439',
    'nombre': 'oscar',
    'cargo': 'supervisor',
    'direccion': ''
}

numero = str(input("numero:"))

controlador = cont.Controlador(agenda)
#crea un nuevo contacto a la lista de usuarios
print(controlador.nuevoContacto(nuevo))
#valida si un contacto existe por su numero de telefono
print(controlador.validarContacto(numero))
#muestra el ultimo contacto sobre el que se actuo
print(controlador.tomarContacto())
#muestra toda la lista de contactos
print(controlador.tomarContactos())
